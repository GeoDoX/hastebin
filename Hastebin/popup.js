const HASTEBIN_URL = "https://hastebin.com/";

// SIZE
const STORAGE_ID_SIZE = "size";
const SIZE_SMALL = { sizeID:"small", width:300, height:250 };
const SIZE_MEDIUM = { sizeID:"medium", width:400, height:300 };
const SIZE_LARGE = { sizeID:"large", width:500, height:350 };

// THEME
const STORAGE_ID_THEME = "theme";
const THEME_CLASSIC = { themeID:"classic", logoImage:"/images/hasteLogo.png", saveImage:"/images/hasteSave.png", historyImage:"/images/hasteHistory.png", settingsImage:"/images/hasteSettings.png", returnImage:"/images/hasteReturn.png", themeColor: "rgb(200, 240, 255)" };
const THEME_LIGHT = { themeID:"light", logoImage:"/images/hasteLogo_w.png", saveImage:"/images/hasteSave_w.png", historyImage:"/images/hasteHistory_w.png", settingsImage:"/images/hasteSettings_w.png", returnImage:"/images/hasteReturn_w.png", themeColor: "white" };

// HISTORY
const STORAGE_ID_HISTORY = "history";
var hasteHistory = [];

var size = SIZE_MEDIUM;
var theme = THEME_CLASSIC;

var placeholderText;

document.addEventListener('DOMContentLoaded', onDOMLoaded, false);

function onDOMLoaded()
{
    document.addEventListener("keyup", onKeyUp);
    placeholderText = getElementByID("hasteContent").placeholder;

    getElementByID("hasteLogo").addEventListener("mouseover", function() { handleMouseEnter(this.id); });
    getElementByID("hasteLogo").addEventListener("mouseout", function() { handleMouseLeave(this.id); });

    getElementByID("hasteSave").addEventListener("mouseover", function() { handleMouseEnter(this.id); });
    getElementByID("hasteSave").addEventListener("mouseout", function() { handleMouseLeave(this.id); });

    getElementByID("hasteHistory").addEventListener("mouseover", function() { handleMouseEnter(this.id); });
    getElementByID("hasteHistory").addEventListener("mouseout", function() { handleMouseLeave(this.id); });

    getElementByID("hasteSettings").addEventListener("mouseover", function() { handleMouseEnter(this.id); });
    getElementByID("hasteSettings").addEventListener("mouseout", function() { handleMouseLeave(this.id); });

    getElementByID("hasteReturn").addEventListener("mouseover", function() { handleMouseEnter(this.id); });
    getElementByID("hasteReturn").addEventListener("mouseout", function() { handleMouseLeave(this.id); });

    getElementByID("hasteContent").addEventListener("focus", function() { getElementByID(this.id).placeholder = "" });
    getElementByID("hasteContent").addEventListener("blur", function() { getElementByID(this.id).placeholder = placeholderText });
    getElementByID("hasteSave").addEventListener("click", handleSaveClick);

    getElementByID("hasteHistory").addEventListener("click", function() { changeView(this.id); });
    getElementByID("hasteSettings").addEventListener("click", function() { changeView(this.id); });
    getElementByID("hasteReturn").addEventListener("click", function() { changeView(this.id); });

    getElementByID("choiceSize").addEventListener("change", sizeChanged);
    getElementByID("choiceTheme").addEventListener("change", themeChanged);

    readUserSettings();
}

function readUserSettings()
{
    readUserSize();
    readUserTheme();
    readUserHistory();
}

function readUserSize()
{
    chrome.storage.sync.get(STORAGE_ID_SIZE, function(result)
    {
        var checkSize = result[STORAGE_ID_SIZE];

        if (!checkSize)
            writeSizeSetting();
        else
        {
            switch (checkSize)
            {
                case SIZE_SMALL.sizeID:
                    size = SIZE_SMALL;
                    break;
                case SIZE_MEDIUM.sizeID:
                    size = SIZE_MEDIUM;
                    break;
                case SIZE_LARGE.sizeID:
                    size = SIZE_LARGE;
                    break;
            }
        }

        applySizeSetting();
    });
}

function readUserTheme()
{
    chrome.storage.sync.get(STORAGE_ID_THEME, function(result)
    {
        var checkTheme = result[STORAGE_ID_THEME];

        if (!checkTheme)
            writeThemeSetting();
        else
        {
            switch (checkTheme)
            {
                case THEME_CLASSIC.themeID:
                    theme = THEME_CLASSIC;
                    break;
                case THEME_LIGHT.themeID:
                    theme = THEME_LIGHT;
                    break;
            }
        }

        applyThemeSetting();
    });
}

function readUserHistory()
{
    chrome.storage.sync.get(STORAGE_ID_HISTORY, function(result)
    {
        var checkHistory = result[STORAGE_ID_HISTORY];

        if (!checkHistory)
            writeUserHistory();
        else
            hasteHistory = checkHistory;
    });
}

function writeUserSettings()
{
    writeSizeSetting();
    writeThemeSetting();
}

function writeSizeSetting()
{
    var sizeObj = {};
    sizeObj[STORAGE_ID_SIZE] = size.sizeID;

    chrome.storage.sync.set(sizeObj, function(){});
}

function writeThemeSetting()
{
    var themeObj = {};
    themeObj[STORAGE_ID_THEME] = theme.themeID;

    chrome.storage.sync.set(themeObj, function(){});
}

function writeUserHistory()
{
    var historyObj = {};
    historyObj[STORAGE_ID_HISTORY] = hasteHistory;

    console.log(hasteHistory);

    chrome.storage.sync.set(historyObj, function(){});
}

function applySizeSetting()
{
    getElementByID("body").style.width = size.width + "px";
    getElementByID("content").style.height = size.height + "px";
    getElementByID("history").style.height = size.height + "px";
    getElementByID("settings").style.height = size.height + "px";
}

function applyThemeSetting()
{
    getElementByID("hasteLogo").src = theme.logoImage;
    getElementByID("hasteSave").src = theme.saveImage;
    getElementByID("hasteHistory").src = theme.historyImage;
    getElementByID("hasteSettings").src = theme.settingsImage;
    getElementByID("hasteReturn").src = theme.returnImage;

    getElementByID("settingsNote").style.color = theme.themeColor;

    var fieldSetElements = getElementsByClass("fieldSet");

    for(var fieldSetIndex = 0; fieldSetIndex < fieldSetElements.length; fieldSetIndex++)
        fieldSetElements[fieldSetIndex].style.borderColor = theme.themeColor;

    var legendElements = getElementsByClass("legend");

    for(var legendElementIndex = 0; legendElementIndex < fieldSetElements.length; legendElementIndex++)
        legendElements[legendElementIndex].style.color = theme.themeColor;

    var labelElements = getElementsByClass("label");

    for(var labelElementIndex = 0; labelElementIndex < labelElements.length; labelElementIndex++)
        labelElements[labelElementIndex].style.color = theme.themeColor;
}

function removeHistoryItem(link)
{
    for(var i = 0; i < hasteHistory.length; i++)
    {
        if (hasteHistory[i].hasteLink === link)
            hasteHistory.splice(i, 1);
    }
}

function onKeyUp(event)
{
    // Save: Check if the 'S' key was pressed
    if (event.keyCode == 83 && event.ctrlKey)
    {
        event.preventDefault();
        if (contentViewVisible())
            hasteContent();
    }
    // History: Check if the 'H' key was pressed
    else if (event.keyCode == 72 && event.ctrlKey)
    {
        event.preventDefault();
        changeView("hasteHistory");
    }
    // Settings: Check if the 'O' key was pressed
    else if (event.keyCode == 79 && event.ctrlKey)
    {
        event.preventDefault();
        changeView("hasteSettings");
    }
    // Return: Check if the 'Backspace' key was pressed
    else if (event.keyCode == 8 && event.ctrlKey)
    {
        event.preventDefault();
        changeView("hasteReturn");
    }
}

function handleSaveClick()
{
    if (contentViewVisible())
        hasteContent();
}

function changeView(elementID)
{
    switch (elementID)
    {
        case "hasteHistory":
            getElementByID("history").style.display = "block";
            getElementByID("settings").style.display = "none";
            getElementByID("content").style.display = "none";
            onHistoryShown();
            break;
        case "hasteSettings":
            getElementByID("history").style.display = "none";
            getElementByID("settings").style.display = "block";
            getElementByID("content").style.display = "none";
            onSettingsShown();
            break;
        case "hasteReturn":
            getElementByID("history").style.display = "none";
            getElementByID("settings").style.display = "none";
            getElementByID("content").style.display = "block";
            onContentShown();
            break;
    }
}

function sizeChanged(selectedSize)
{
    switch (selectedSize.target.value)
    {
        case SIZE_SMALL.sizeID:
            size = SIZE_SMALL;
            break;
        case SIZE_MEDIUM.sizeID:
            size = SIZE_MEDIUM;
            break;
        case SIZE_LARGE.sizeID:
            size = SIZE_LARGE;
            break;
    }

    applySizeSetting();
    writeSizeSetting();
}

function themeChanged(selectedTheme)
{
    switch (selectedTheme.target.value)
    {
        case THEME_CLASSIC.themeID:
            theme = THEME_CLASSIC;
            break;
        case THEME_LIGHT.themeID:
            theme = THEME_LIGHT;
            break;
    }

    applyThemeSetting();
    writeThemeSetting();
}

function handleMouseEnter(elementID)
{
    switch (elementID)
    {
        case "hasteLogo":
            getElementByID(elementID).src = getOppositeTheme().logoImage;
            break;
        case "hasteSave":
            getElementByID(elementID).src = getOppositeTheme().saveImage;
            break;
        case "hasteHistory":
            getElementByID(elementID).src = getOppositeTheme().historyImage;
            break;
        case "hasteSettings":
            getElementByID(elementID).src = getOppositeTheme().settingsImage;
            break;
        case "hasteReturn":
            getElementByID(elementID).src = getOppositeTheme().returnImage;
            break;
    }
}

function handleMouseLeave(elementID)
{
    switch (elementID)
    {
        case "hasteLogo":
            getElementByID(elementID).src = theme.logoImage;
            break;
        case "hasteSave":
            getElementByID(elementID).src = theme.saveImage;
            break;
        case "hasteHistory":
            getElementByID(elementID).src = theme.historyImage;
            break;
        case "hasteSettings":
            getElementByID(elementID).src = theme.settingsImage;
            break;
        case "hasteReturn":
            getElementByID(elementID).src = theme.returnImage;
            break;
    }
}

function getOppositeTheme()
{
    switch (theme.themeID)
    {
        case THEME_CLASSIC.themeID:
            return THEME_LIGHT;
        case THEME_LIGHT.themeID:
            return THEME_CLASSIC;
    }
}

function settingsViewVisible()
{
    return getElementByID("settings").style.display == "block";
}

function historyViewVisible()
{
    return getElementByID("history").style.display == "block";
}

function contentViewVisible()
{
    return getElementByID("content").style.display == "block";
}

function onHistoryShown()
{
    getElementByID("hasteHistory").style.display = "none";

    getElementByID("hasteSave").style.display = "none";
    getElementByID("hasteSettings").style.display = "none";

    getElementByID("hasteReturn").style.display = "inline";

    var historyList = getElementByID("historyList");
    var historyItem;
    var historyItemLink;
    var historyItemLinkRemove;

    while (historyList.firstChild)
        historyList.removeChild(historyList.firstChild);

    for(var i = 0; i < hasteHistory.length; i++)
    {
        historyItem = document.createElement("li");
        historyItemLink = document.createElement("a");
        historyItemLinkRemove = document.createElement("button");

        historyItem.className = "historyListItem";

        historyItemLink.href = HASTEBIN_URL + hasteHistory[i].hasteLink;
        historyItemLink.textContent = hasteHistory[i].hasteLink;
        historyItemLink.title = "[" + hasteHistory[i].hasteDate + "]";
        historyItemLink.className = "historyListItemLink";

        historyItemLink.addEventListener("click", function() { openInNewTab(this.href); });

        historyItemLinkRemove.textContent = "Remove";
        historyItemLinkRemove.className = "historyListItemLinkRemove";

        historyItemLinkRemove.addEventListener("click", function()
        {
            historyList.removeChild(this.parentNode);
            removeHistoryItem(this.parentNode.firstChild.textContent);
            writeUserHistory();
        });

        historyItemLink.style.color = theme.themeColor;
        historyItem.style.color = getOppositeTheme().themeColor;

        historyItem.appendChild(historyItemLink);
        historyItem.appendChild(historyItemLinkRemove);
        historyList.appendChild(historyItem);
    }
}

function onSettingsShown()
{
    getElementByID("hasteSettings").style.display = "none";

    getElementByID("hasteSave").style.display = "none";
    getElementByID("hasteHistory").style.display = "none";

    getElementByID("hasteReturn").style.display = "inline";

    var sizeSelectedIndex;
    var themeSelectedIndex;

    switch (size.sizeID)
    {
        case SIZE_SMALL.sizeID:
            sizeSelectedIndex = 0;
            break;
        case SIZE_MEDIUM.sizeID:
            sizeSelectedIndex = 1;
            break;
        case SIZE_LARGE.sizeID:
            sizeSelectedIndex = 2;
            break;
    }

    switch (theme.themeID)
    {
        case THEME_CLASSIC.themeID:
            themeSelectedIndex = 0;
            break;
        case THEME_LIGHT.themeID:
            themeSelectedIndex = 1;
            break;
    }

    getElementByID("choiceSize").selectedIndex = sizeSelectedIndex;
    getElementByID("choiceTheme").selectedIndex = themeSelectedIndex;
}

function onContentShown()
{
    getElementByID("hasteSave").style.display = "inline";
    getElementByID("hasteHistory").style.display = "inline";
    getElementByID("hasteSettings").style.display = "inline";
    getElementByID("hasteReturn").style.display = "none";
}

function hasteContent()
{
    haste(getElementByID("hasteContent").value);
}

function haste(content)
{
    if (!content)
        return;

    var hasteRequest = new XMLHttpRequest();

    hasteRequest.addEventListener("load", getHasteURL);

    // Make a POST request to Hastebin, and send it the content
    hasteRequest.open("POST", HASTEBIN_URL + "documents");
    hasteRequest.send(content);
}

function getHasteURL()
{
    // Get the hastebin url back from the request as a JSON, and parse it into the full URL
    var hasteJSON = JSON.parse(this.responseText);
    var hasteURL = HASTEBIN_URL + hasteJSON.key;

    hasteHistory.push(new HasteHistoryItem(getCurrentDateTime(), hasteJSON.key));
    writeUserHistory();

    // Open it in a new tab
    openInNewTab(hasteURL);
}

function openInNewTab(url)
{
    var a = document.createElement("a");
    a.target = "_blank";
    a.href = url;
    a.click();
}

function getCurrentDateTime()
{
    var now = new Date();
    return [[addZero(now.getDate()),
        addZero(now.getMonth() + 1),
        now.getFullYear()].join("/"),
        [addZero(now.getHours()),
            addZero(now.getMinutes())].join(":"),
        now.getHours() >= 12 ? "PM" : "AM"].join(" ");
}

function addZero(num)
{
    return (num >= 0 && num < 10) ? "0" + num : num + "";
}

function getElementByID(id)
{
    return document.getElementById(id);
}

function getElementsByClass(className)
{
    return document.getElementsByClassName(className);
}

function HasteHistoryItem(date, link)
{
    this.hasteDate = date;
    this.hasteLink = link;
}